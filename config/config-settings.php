<?php

return [


    'route' => [
        'prefix' => '/',
        'middleware' => ['web', 'auth']
    ],


    'default_page' => 'primary',


    'pages' => [

        'primary' => [
            'title' => 'Primary Settings',
        ],

    ],


    'groups' => [

        'application' => [
            'page' => 'primary',
            'title' => 'Application',
            'description' => 'The major settings to customize your application\'s core functionality.'
        ],

        'mail' => [
            'title' => 'Email & SMTP Configuration',
            'description' => 'Email & SMTP configuration to be used to send emails.'
        ]
    ],


    'fields' => [

        // Application --------------------

        'app.name' => [
            'group' => 'application',
            'type' => 'text',
            'title' => 'Name',
            'description' => 'The application name is the primary title that will be used to identify this application',
        ],

        'app.description' => [
            'group' => 'application',
            'type' => 'text',
            'title' => 'Description',
            'description' => 'The application description or the tagline.',
        ],

        'app.timezone' => [
            'group' => 'application',
            'type' => 'text',
            'title' => 'Time Zone',
            'description' => 'Supported Timezone string.',
        ],

        'app.locale' => [
            'group' => 'application',
            'type' => 'text',
            'title' => 'Default Locale',
            'description' => 'Supported locale string to use by default for translation.',
        ],

        'app.fallback_locale' => [
            'group' => 'application',
            'type' => 'text',
            'title' => 'Fallback Locale',
            'description' => 'The locale to use when the translation is not available for current locale.',
        ],



        // Mail ------------------------

        'mail.host' => [
            'group' => 'mail',
            'type' => 'text',
            'title' => 'SMTP Host',
        ],

        'mail.port' => [
            'group' => 'mail',
            'type' => 'text',
            'title' => 'SMTP Port',
        ],

        'mail.encryption' => [
            'group' => 'mail',
            'type' => 'text',
            'title' => 'Encryption',
            'description' => 'SMTP Encryption eg: tls, ssl. Leave blank if not encrypted.',
        ],

        'mail.username' => [
            'group' => 'mail',
            'type' => 'text',
            'title' => 'Username',
            'description' => 'SMTP Username.',
        ],

        'mail.password' => [
            'group' => 'mail',
            'type' => 'text',
            'title' => 'Password',
            'description' => 'SMTP Password.',
        ],

        'mail.from.address' => [
            'group' => 'mail',
            'type' => 'text',
            'title' => 'From Email',
            'description' => 'Emails will be sent via this email.',
        ],

        'mail.from.name' => [
            'group' => 'mail',
            'type' => 'text',
            'title' => 'From Name',
            'description' => 'Name to use in From address.',
        ],

    ]

];
