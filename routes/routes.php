<?php

use \Illuminate\Support\Facades\Route;
use \MVCommerceModules\ConfigSettings\Controllers\ConfigSettingsController;



$middleware = config('mvcommerce.config-settings.route.middleware', ['web', 'auth']);
$prefix = config('mvcommerce.config-settings.route.prefix', '/');

Route::middleware($middleware)
    ->prefix($prefix)->group(function(){
        Route::resource('settings', ConfigSettingsController::class)
            ->only(['index', 'store']);
    });
