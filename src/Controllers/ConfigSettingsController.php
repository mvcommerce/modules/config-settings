<?php

namespace MVCommerceModules\ConfigSettings\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use MVCommerceModules\ConfigSettings\Facades\ConfigSettingsFacade as CSFacade;


class ConfigSettingsController extends Controller
{

    public function index(){

        $pages = CSFacade::fieldsInPages();

        return view('mvcommerce.config-settings::index',
            compact('pages')
        );

    }


    public function store(Request $request){

        $fields = CSFacade::fields();
        $keys = array_keys($fields);

        $data = $request->only($keys);

        foreach ( $data as $key => $settings ){
            CSFacade::set($key, $settings);
        }


        return back();

    }


}
