<?php

namespace MVCommerceModules\ConfigSettings\Providers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use MVCommerceModules\ConfigSettings\Facades\ConfigSettingsFacade as CSFacade;
use MVCommerceModules\ConfigSettings\Models\ConfigSetting;
use MVCommerceModules\ConfigSettings\ConfigSettings;

class ConfigSettingsServiceProvider extends ServiceProvider
{



    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){


        $this->loadRoutesFrom(__DIR__.'/../../routes/routes.php');

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');

        $this->publishes([
            __DIR__.'/../../migrations' => database_path('migrations'),
        ], ['mvcommerce', 'mvcommerce_config_settings', 'mvcommerce_mvcommerce_config_settings_migrations', 'migrations']);


        $this->publishes([
            __DIR__ . '/../../config/config-settings.php' => config_path('mvcommerce/config-settings.php'),
        ], ['mvcommerce', 'mvcommerce_config_settings', 'mvcommerce_config_settings_config', 'config']);


        $this->loadViewsFrom(__DIR__.'/../../views', 'mvcommerce.config-settings');

        $this->publishes([
            __DIR__.'/../../views' => resource_path('views/vendor/mvcommerce.config-settings'),
        ], ['mvcommerce', 'mvcommerce_config_settings', 'mvcommerce_config_settings_views', 'views']);


        $this->mergeConfigWithSettings();

    }


    /**
     * Register the application services.
     *
     * @return void
     * @throws \Throwable
     */
    public function register()
    {

        $this->app->bind('mvcommerce.config-settings', ConfigSettings::class);


        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config-settings.php', 'mvcommerce.config-settings'
        );

    }



    public function mergeConfigWithSettings(){

        try{

            DB::connection()->getPdo();

            $table = with(new ConfigSetting())->getTable();
            if( !Schema::hasTable( $table ) ) return;

            CSFacade::loadSettings();

        }catch (\Exception $e){

        }

    }


}
